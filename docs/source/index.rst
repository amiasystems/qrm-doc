.. Quick Response Manufacturing documentation master file, created by
   sphinx-quickstart on Tue Jan 13 09:56:13 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. URL Links

.. _It's About Time: http://astore.amazon.fr/i14ynet-21?_encoding=UTF8&node=33

.. Substitution of images

.. |it_s_about_time.jpg| image:: _static/img/it_s_about_time.jpg
			:alt: It's About Time: The Competitive Advantage of Quick Response Manufacturing – 7 April 2010 de Rajan Suri



########################################################################
Welcome to Quick Response Manufacturing's documentation!
########################################################################

:abbr:`QRM (Quick Response Manufacturing)` is a method that emphasizes the beneficial effect of reducing internal and external lead times.

This paper is a summary of the book "`It's About Time`_: The Competitive Advantage of Quick Response Manufacturing" by Rajan Suri.
The sole aim of this paper is to summarise the finding within the book as training material for our teams and customers.

Contact us: http://www.amia-systems.com for any inquiry.

.. note::
    All readers should buy the book "`It's About Time`_" as those notes would be hard to digest in other circumstances.

|it_s_about_time.jpg|


****************************************
4 core concepts
****************************************

* The power of time
* Organization structure
* system dynamics
* enterprise-wide application

****************************************
A bit of history
****************************************

21st century: increase demand for low-volume, high variety products with options configured for individual customers or even custom-engineered for each client.

:term:`Takt time` and :term:`Kanban` eliminates variability operations.

****************************************
Wrong believes p9
****************************************

**Everyone has to work harder** => reduce the lead-time

In average, the :term:`touch time` is 5% to 1%  of the complete process => Reducing the costs :abbr:`ABC (Activity Based Costing)` does not reduce the lead-time!

*******************************************
The Manufacturing Critical-Path Time p12
*******************************************


The :abbr:`MCT (Manufacturing Critical-Path Time)`

* Collect data without being overboard by lots of data
* Pay attention to the calendar time: start with the creation of the order
    * include order processing, materials planning, scheduling, manufacturing logistics and quantifies waste in system

p16 : :term:`MCT` includes information such as flow delay, planning time, raw material stocks, suppliers lead-times, logistics time

p18: :term:`VSM`: "current state" map has lot of details and it could be hard to get insight on where big opportunities lies "future state" is not immediately clear where improvements have been made.

p24: Reducing :term:`MCT` will increase the "greys areas" and increase the direct labor costs but reduce warehouse and indirect costs

p13: Make :term:`MCT` reduction the main performance measure throughout your organization and also for your suppliers

###################################################################################
Chapter II: Organizational structure of QRM: QRM Cells, Teamwork and Ownership p35
###################################################################################

* cost-based approach focuses on reducing the :term:`touch time`
* The :term:`MCT` is 100 when the touch time is 5!

*****************************************
4 structural changes p48
*****************************************

1. From functional to cellular
2. From specialized narrowly focused workers to a cross-trained workforce
3. From Top-Down control to team ownership
4. From efficiency/utilization to :term:`MCT` reduction

:abbr:`FTMS (Focused Target Market Segment)`

p50: A :term:`QRM Cell` is a dedicated, collocated, multifunctional resources selected so that this set a complete sequence of operations for all jobs belonging to a specified :term:`FTMS`. The set of resources include a team of cross-trained people that has complete ownership of the cell's operations. The primary goal of the :term:`QRM Cell` is the reduction of the :term:`MCT`

p51:

* The resources assigned to a QRM Cell are **fully dedicated** to that cell.
* The resources that form a QRM Cell must also be located in close proximity to each other in an area that is clearly demarcated as belonging to the cell

*******************************************************************************************
How to ensure QRM Cells they aren't unexpected bottlenecks and resulting in job delays? p54
*******************************************************************************************

1. Teamwork
2. Cross Training
3. Choice of metric
4. QRM Approach to capacity planning

*******************************************************************************************
The metrics p60
*******************************************************************************************

.. math:: \text{QRM Number = }\frac{\text{Base period MCT}}{\text{Current period MCT}}*100
    :label: QRMNumber

.. math:: \text{For example: QRM Number for the base period = }\frac{\text{12 days}}{12}=100
.. math:: \text{QRM Number if a first improvement is made }\frac{\text{12 days}}{10}=120
.. math:: \text{QRM Number if a second improvement is made }\frac{\text{12 days}}{8}=150

===================================
Which metrics should you use?
===================================

* The Cell's :term:`MCT`

.. note::
    :term:`QRM Number` effectively measure the MCT reduction. Psychologically, the equation :eq:`QRMNumber` increases as the MCT is redirected and motivate the teams

.. note::
    QRM number increase faster when the gain is bigger. i.e. Gain = :math:`\frac{\text{2 days}}{12}=17\%` or :math:`\frac{\text{2 days}}{8}=25\%`


******************************************************************
Prerequisites p63
******************************************************************

1. Define the start and stop conditions "when the clock starts ticking"
2. the clock starts when the team has full ownership of that time period
3. Choose carefully the unit depending if you want to reduce the :term:`lead-time for orders` or :term:`lead-time to stock`

.. attention::

    p66: Lean manufacturing is not well suited to the then highly customized highly customized and low-volume business

.. sidebar:: Data model

    Useful for the dev of an application

* Two products families as the :term:`FTMS` for a first cell.

******************************************************************
Process to setup a cell p66
******************************************************************

1. Select the product that requires similar process and are relatively self contained and use the same collections of resources
2. Setup an implementation team whose goal is to design the cell and implement it. The team is recruited from all functional areas (see the :term:`Porter Value Chain`)
3. The team task were to come up with an initial design of the cell and the designated area for it
4. Use projections before/after metrics to justify the financial investment needed
5. recruit the cell operator and train them
6. Move the needed machines into the cells
7. Launch the cell
8. Support it until it was operating successfully
    1. To recruit the cell operators, produce colorful flyers on bulletin board presenting an exciting career
    2. Skill needed: attitude, enthusiasm for learning, willingness to engage in team building and teamwork

##############################################################################
Chapter III: Understanding and exploiting system dynamics
##############################################################################

Machine <-> People <-> Product <-> Machine

* Rethink the capacity planning approach
* lot sizing policies

p74: Read the Case of "Landing Gears"

p75: Case of situations in the supermarket, airport checking, taxi

******************************************************************
Magnifying effect of utilization
******************************************************************

In :term:`QRM`, :term:`utilization` is the ratio of the total time that the machines is occupied for any task over the Total time the factory is scheduled at work. See :eq:`utilization`

.. math:: Utilization = \frac{\text{Total time that the machines is occupied for any task}}{\text{The Total time the factory is scheduled at work}}
    :label: Utilization

.. math:: \text{QRM Utilization  =} \frac{\text{11h breaks down + 104h making parts + 26h setup + 3h maintenance}}{\frac{8h}{d}*20d}=\frac{144 h}{160 h}
    :label: UtilizationQRMSample

.. math:: \text{ABC Utilization =} \frac{\text{104h making parts}}{\frac{8h}{d}*{20d}}=\frac{104h}{160h}
    :label: UtilizationABCSample

p78: The :term:`flow time` of a job arriving at a resource as the average time that it takes for that resource to finish other work that may be ahead of this job, start working on this job and finally complete it

p79:

.. math:: \text{u is a decimal number = 0.9 = }90\% \text{of utilization}
    :label: U

.. math:: \text{M = Magnifying effect of utilization in days} = \frac{u}{1-u} = \frac{0.9}{1-0.9} = \text{9 days}
    :label: Magnifying effect of utilization in days

:term:`M` shows how you queue times will blow up as you try to push your utilization higher. Example: :math:`75\%\text{ utilization} => \frac{0.75}{1-0.75} = \text{3 days delay before to start a, unexpected job}`

p82: The concept of variability in arrival terms of jobs to resource. i.e. Does the job arrives at the work center approximately every 4 hours or in a random manner?

The concept of variability in the time that it takes for the resource to work on each job.

:math:`\text{setup time + total time to work on all work pieces in the job.}`

***************************
2 types of variability p83
***************************

1. Arrival time variability
2. Job time variability


.. math:: AV=\text{Average variability } = \frac{ATV^2+JTV^2}{2}

.. math:: TJ=\text{Average time for a job} = \text{setup time + time to work on all the work pieces}

.. math:: QT=\text{Queue time} = \text{Average time the job waits in queue}

.. math:: QT=AV*M*TJ

.. math:: \text{Flow time}=(AV*M*TJ)+TJ = \text{Average queue time + average time to perform the job}

********************************************
How to reduce the :term:`MCT` for jobs?
********************************************

==================================================================
Reduce the AV, Average Variability p84
==================================================================

* Read 6 tips to reduce the availability in arrivals
* Read 7 tips to reduce the variability in job times


==================================================================
Reduce the M, Magnifying effect of utilization p85
==================================================================

* QRM proposes that resources aren't loaded up beyond 85%
* Read the 7 tips to reduce utilization and create spare capacity


==================================================================
Reduce the TJ, The time per job p92
==================================================================

* Time based approach to batch sizing

The resources are fixed (machines and people only the batch size varied

==================================================================
How to find the batch size
==================================================================

1. Batch sizes in use at most companies are invariability too large
    1. But don't reduce the batch size if any of your products go through a resource that is a severe bottleneck
    2. Reduce the batch size in small steps
2. Use a formula in Appendix D (simple version)
3. Analysis of specific data for the resource and the details are in Appendix D
4. Use of complex dynamic modeling software


**********************
QRM Principles p93
**********************

1. Create the right organization structure
2. Use the concept of :term:`FTMS` and organize into QRM Cells
3. Don't try to reduce the batch size within the existing organization structure
4. Find the right batch size after the reorganization
5. :term:`Alleviate` the bottlenecks after the decrease of the batch size

==================================================================
Setup reduction - insight from the system dynamics p95
==================================================================

Impact of batch size reduction combined with setup reduction
You need to add capacity at few resources and reduce :term:`M` to enable the batch size reduction

***********************************************************************************************************************
Reexamine organizational policies to support the batch size reduction (material management, sales, accounting)
***********************************************************************************************************************

* Material management policies
    * Parts being made to stock, consider making smaller quantities
* Sales policies
    Reduce the batch sizes
* Costing policies
    Setup time redirection and overhead allocation reduction

**************************************************************************************************************
QRM approach to transfer batching
**************************************************************************************************************

First priority for the teams should be the reduction of overall batch size along with setup reduction or other supporting improvements if needed and the transfer batching should come in only after those efforts are in place. :term:`POLCA` is used to transfer parts between cells, helping the material control strategy

********************************************************************************
System dynamics compared with MRP, EOQ and other traditional approaches p99
********************************************************************************

:term:`MRP` material requirement planning
:term:`MRPII` manufacturing resource planning
:term:`ERP` Enterprise Resource planning

***********************************************************************************************************************
Why companies mistakenly invest in warehouse instead of machines? p102
***********************************************************************************************************************

Case study aerospace industry

* high variability in their operations
* high utilization
* incentive to run large batch sizes => long lead-times => necessitate large finished goods stock

########################################################################
Chapter IV: A unified strategy for the whole industry
########################################################################

******************************************************
Principles of quick response in office operations p105
******************************************************

* See Appendix A
* Shift from cost-based thinking to the time-based thinking

Apply 4 principles

1. Reorganize functional departments into :abbr:`Q-ROCs (Quick Response Office Cells)` (read Queue rocks)
2. Move from Top-down control to ownership by the :term:`Q-ROCs`
3. Progress from specialized to cross-trained team members in the :term:`Q-ROCs`
4. Replace your efficiency/utilization goals by :term:`MCT` reduction measured via the `QRM Number` for each :term:`Q-ROCs`

******************************************************************
How to identify an FTMS in the context of office operations p112
******************************************************************

1. Market; an internal or external customer that requires shorter response time
2. Examine office-processing steps for this segment and be sure to identify the product of these steps
3. Is there a subset of this product that can be attacked with simpler processing steps
4. See if the subset represents a significant enough demand that it is pursuing as a focus
5. Proceed with the design of the :term:`Q-ROC`

********************************************
Who does the job?
********************************************

A cross-functional group that includes people from Marketing & Sales, engineering, purchasing departments

* See Appendix B for the brainstorming

********************************************
How to form effective Q-ROCs? p114
********************************************

1. Cut through functional boundaries buy forming :term:`Q-ROCs` which is :term:`closed-loop`, :term:`collocated`, dedicated multifunctional, cross-trained team responsible for the office processing of all jobs belonging to a specific FTMS
2. The team has complete ownership
3. Primary goal of the team is the cell's :term:`MCT`
4. Provide team members with expertise via cross training
5. Empower the team to make necessary decisions in order to server the particular FTMS
6. Change reporting structures as people aren't separated functionally
7. Eliminate steps that become useless due to the cross-functional team (p116)
8. Redesign the steps p117


********************************************
Q-ROC design method p121
********************************************

1. Strategically plan for spare capacity
2. Reduce variability in terms of both job arrivals time and for tasks performed
3. Convert multiple tasks from being sequential to overlapping or parallel
4. Minimize batching by creating standardized templates or forms people will do the task right away

********************************************
Marketing argument p122
********************************************

A major advantage that this has over applying QRM to the shop floor is that moving people and desks is much less costly than moving machines and creating shop floor cells

.. note::
    Use the case study Alexandria Extrusion Company


********************************************
Restructure your MPS to support QRM
********************************************

Align your :term:`MRP` structure with your :term:`QRM` strategy

1. Restructure  the organization into QRM Cells
2. Use :term:`MRP` for higher level planning of material flow from suppliers and between cells
3. Complement with :term:`POLCA`, the :term:`QRM` material control method that coordinates flows between cells in order to meet delivery dates

================================================================================================
Implement a high-level MRP (HL/MRP) p125
================================================================================================

1. Create QRM Cells
2. As you use cells for various parts, rethink the :abbr:`BOM (Bill of Material)` for this new structure of production with the goal of reducing :term:`MCT`
    * Rethink design
    * material choices
    * and make/buy decisions to flatten bills of material and eliminate operations
3. Use :term:`HL/MRP` to plan flow of materials from suppliers and across cells in order to meet delivery dates
4. Inform the cells teams of the expected start and finish dates for the part and it will manage the details of how and when the operations would be performed
5. The cells teams communicate with the planner as conditions change
6. The :term:`POLCA` system is used to coordinate the flow between cells if a part needs to go through multiple cells
    * The :term:`POLCA` uses actually your existing :term:`MRP` system and build on it to make it more effective

************************************************
POLCA - The shop flow material control strategy
************************************************

:term:`Kanban` is designed for shop floor coordination and control of high-volume production of similar product and with a fairly stable demand
:term:`Takt time` cycle time within which that operation must be completed in order to keep up with the average rate of sales

:abbr:`POLCA (Paired-Cell Overlapping Loops of Cards with Authorization)`

##########################
Paired-Cell Loops of Cards
##########################

1. If material flows between 2 cells, then they are connected by a `POLCA Loop`
2. POLCA Cards circulate in the loop
    * these cards are specific to the loop and are labeled based on the origin and the destination cell i.e. `A/B Card`
3. If a card is available, the job is started and the card is kept with the job into `Cell A`
4. When `Cell A` completes the job, it sends the job along with the `A/B Card` to `Cell B`
5. When `Cell B` finishes working on this job, the `A/B Card` is sent to `Cell A`
6. Returning `POLCA Card` signify the availability of capacity in downstream cells

.. note:: POLCA is a capacity signal

##########################
Authorization
##########################

Based on the :term:`HL/MRP`, a job can start if start date is today or earlier but not dates of tomorrow or later


##########################
Dispatch list p134
##########################

1. Take the first authorized job on the list
2. The team looks up where this job is going after their cell
3. The cell team checks to see if they have a `A/X POLCA Card`
4. If the POLCA Card is available, they start the job
5. If the POLCA Card is not available they skip the job

##########################
Strict scheduling rules
##########################

Every time you put capacity into a job that is not needed

1. You steal capacity from another job that might have needed it
2. You create more :abbr:`WIP (Work In Progress)`
3. You add to time to your :term:`MCT`

##########################
Overlapping loops p138
##########################

You can see that except in the first and last cells in routing, a job will always have 2 POLCA Cards with it when it is being actively worked on in any of the intermediate cells in its routing

Advantages:

1. This feature recognizes that each cell in the routing is a supplier or a customer to another cell
2. The jobs with problems are not continually pushed aside in favor of starting new jobs

####################################################
Benefits realized with POLCA implementations
####################################################

TO BE FILLED SOON

********************************************
Glossary
********************************************

.. glossary::

    Alleviate
        Make less severe

    Closed-loop
        When the job enter the Q-ROC, the team has both expertise and authority to complete the processing of those jobs

    Collocated
        Physically locating the team members together in one area. Moving the team members our of their departments and into this new area sends a strong message to the whole organization

    ERP
        Enterprise resource planning (ERP) is a business management software—usually a suite of integrated applications—that a company can use to collect, store, manage and interpret data from many business activities, including: Product planning, cost. Manufacturing or service delivery. Marketing and sales.

    Flow time
        Flow time of a job arriving at a resource as the average time that it takes for that resource to finish other work that may be ahead of this job, start working on this job and finally complete it

    FTMS
        Focused Target Market Segment, Clear choice of the area where we want to make a start with the implementation of QRM.

    HL/MRP
        High Level :term:`MRP` performs the planning The higher level MRP (HL/MRP) has the following functions:
            1. Prediction of the need for, and ordering materials from external suppliers;
            2. Coordination of material delivery across internal cells.


    Kanban
        Method designed for shop floor coordination and control of high-volume production of similar product and with a fairly stable demand

    Lead-time for orders
        Period between placing an order and receiving the ordered item.

    Lead-time to stock
        Period between placing a forecast and stocking the forecasted item. Products are manufactured based on demand forecasts. Since accuracy of the forecasts will prevent excess inventory and opportunity loss due to stockout, the issue here is how to forecast demands accurately.

    M
        Magnifying effect of utilization shows how you queue times will blow up as you try to push your utilization higher. Example: :math:`75\% \text{utilization} => \frac{0.75}{1-0.75} = \text{3 days delay before to start a, unexpected job}`

    MCT
        Manufacturing Critical-Path Time is a precise QRM metric for lead time designed to capture how order processing, manufacturing, raw material procurement and shipping are linked while fulfilling customer orders.

    MRP
        Material requirements planning (MRP) is a production planning, scheduling, and inventory control system used to manage manufacturing processes. Most MRP systems are software-based, while it is possible to conduct MRP by hand as well.

    MRPII
        Manufacturing resource planning (MRP II) is defined as a method for the effective planning of all resources of a manufacturing company. Ideally, it addresses operational planning in units, financial planning, and has a simulation capability to answer "what-if" questions and extension of closed-loop MRP.

    POLCA
        POLCA is a capacity signal

    Porter value chain
        In Porter's value chains, Inbound Logistics, Operations, Outbound Logistics, Marketing and Sales and Service are categorized as primary activities. Secondary activities include Procurement, Human Resource management, Technological Development and Infrastructure

    QRM
        Quick Response Manufacturing is a method that emphasizes the beneficial effect of reducing internal and external lead times.

    QRM Cell
         A dedicated, collocated, multifunctional resources selected so that this set a complete sequence of operations for all jobs belonging to a specified :term:`FTMS`. The set of resources include a team of cross-trained people that has complete ownership of the cell's operations. The primary goal of the :term:`QRM Cell` is the reduction of the :term:`MCT`

    QRM Number
        Effectively measure the MCT reduction. Psychologically, the equation :eq:`QRMNumber` increases as the MCT is redirected and motivate the teams. QRM number increase faster when the gain is bigger. i.e. gain :math:`\frac{\text{2 days}}{12}=17\%` or :math:`\frac{\text{2 days}}{8}=25\%`

    Q-ROC
        See :term:`Q-ROCs`

    Q-ROCs
        Quick Response Office Cells a closed-loop, collocated, multi-functional, cross-trained team responsible for a family of products. It empowers office workers to make necessary decisions. - See more at: http://www.alexandriaindustries.com/qrmnewsletters/qroc.php#sthash.IOj9AObI.dpuf


    Takt time
        Cycle time within which that operation must be completed in order to keep up with the average rate of sales

    Touch time
        The time that a person or a machine is actually working on a product

    Utilization
       The ratio of the total time that the machines is occupied for any task over the Total time the factory is scheduled at work. See the equation :eq:`Utilization`

    VSM
        Value stream mapping is a lean-management method for analyzing the current state and designing a future state for the series of events that take a product or service from its beginning through to the customer. At Toyota, it is known as "material and information flow mapping"




********************************************
Contents:
********************************************

.. toctree::
   :maxdepth: 2


********************************************
Indices and tables
********************************************

* :ref:`genindex`
* :ref:`search`