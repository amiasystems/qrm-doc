```html
   ___        _      _      ____                                        __  __                    __            _              _             
  / _ \ _   _(_) ___| | __ |  _ \ ___  ___ _ __   ___  _ __  ___  ___  |  \/  | __ _ _ __  _   _ / _| __ _  ___| |_ _   _ _ __(_)_ __   __ _ 
 | | | | | | | |/ __| |/ / | |_) / _ \/ __| '_ \ / _ \| '_ \/ __|/ _ \ | |\/| |/ _` | '_ \| | | | |_ / _` |/ __| __| | | | '__| | '_ \ / _` |
 | |_| | |_| | | (__|   <  |  _ <  __/\__ \ |_) | (_) | | | \__ \  __/ | |  | | (_| | | | | |_| |  _| (_| | (__| |_| |_| | |  | | | | | (_| |
  \__\_\\__,_|_|\___|_|\_\ |_| \_\___||___/ .__/ \___/|_| |_|___/\___| |_|  |_|\__,_|_| |_|\__,_|_|  \__,_|\___|\__|\__,_|_|  |_|_| |_|\__, |
                                          |_|                                                                                          |___/                                          
```

# Quick Response Manufacturing

`Quick Response Manufacturing` is a method that emphasizes the beneficial effect of reducing internal and external lead times.

* Learn about Quick Response Manufacturing on [QRM on Wikipedia]

# Sphinx 

## How to build a new html `rm -r build/html & make html && open build/html/index.html`
## How to build a pdf `rm -r build/latex & make latexpdf && open build/latex/QuickRespanufacturing.pdf`

	curl -X POST http://readthedocs.org/build/qrm-doc
	make linkcheck && open build/linkcheck/output.txt 

# use multilingual capabilities including .po 

	easy_install sphinx-intl
	make gettext
	export SPHINXINTL_LANGUAGE=de,en
	sphinx-intl update -p build/locale -d source/
	sphinx-intl build -d source/
	make -e SPHINXOPTS="-D language='de'" html

## Sphinx documentation

* Sphinx (tool to build html, pdf or Qt documentation from a Restructured format)
	* http://sphinx-doc.org/index.html
* Restructured Text format
	* http://docutils.sourceforge.net/docs/user/rst/quickstart.html
	* http://docutils.sourceforge.net/docs/user/rst/quickref.html (**Best documentation when you are confident with rst format**)
	* http://docutils.sourceforge.net/docs/user/rst/cheatsheet.txt
* MathJax
    * http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference
    * http://www.suluclac.com/Wiki+MathJax+Syntax
    * http://sphinx-doc.org/latest/ext/math.html
    * http://en.wikipedia.org/wiki/Help:Displaying_a_formula
    * http://en.wikipedia.org/wiki/MathJax
    * http://www.mathjax.org/

[QRM on Wikipedia]: http://en.wikipedia.org/wiki/Quick_response_manufacturing
[Text ASCII Art Generator]: http://patorjk.com/software/taag/#p=display&h=0&v=0&f=Ivrit&t=Quick%20Response%20Manufacturing